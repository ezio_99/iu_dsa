package assignment2;

// Abuzyar Tazetdinov
// https://codeforces.com/group/3ZU2JJw8vQ/contest/272963/submission/74501592

/*
 At first glance, the problem of crossing two lines seems trivial and useless.
 However, you can use this problem to simulate many situations.
 For example, check whether two objects that are moving in a straight line will collide,
 which can be useful. This can be used in game development, solving physical problems.
 Also, might be useful in simple AI, when we need the decision about changing the path if
 objects will have intersection (in this situation we need some additional modification considering
 simplification of the speed of objects).
*/

/*
 Based on the previous paragraph, we can say that the problem with the intersection of the two segments can be useful.
 However, what should we do if we have more segments than 2?
 The very first thing that comes to mind is to check all the segments with each other and it will take O(n^2) time.
 However, using the sweeping line algorithm, you can solve this problem in O(n*logn).
 In problems of computational geometry, one of the most common methods for solving many problems is the use
 of a sweeping line. In this problem, using this algorithm just gives the answer to whether there is
 an intersection or not, but it doesn't give us all the intersections.

 In the sweep line method, a virtual vertical sweep line is drawn along a given set of geometric objects
 and this line usually moves from left to right (the direction does not matter).
 Sweeping provides a way to organize the properties of objects in a dynamic data structure (usually in the tree).
 This structure gives us the possibility to use relationships between objects.
 In the case with n line segments intersection, we sort the line segment's endpoints from left to right
 and check intersections for every new point using the tree, because the tree gives us better asymptotic bounds.
*/

import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;

/**
 * Class containing SweepLine algorithm implementation.
 * Algorithm taken from Cormen's book.
 *
 * @author Abuzyar Tazetdinov
 */
public class SweepLine {
    /**
     * Class implementing Point for SweepLine algorithm.
     * Point contains reference to second Point in line segment
     * and can be treated as circular array with 2 elements.
     * Therefore no need to create object representing line segment.
     */
    static class Point implements Comparable<Point> {
        /**
         * Point coordinates
         */
        public int x, y;

        /**
         * Shows point position comparing to second point in line segment
         */
        public boolean isLeft = false;

        /**
         * Show point position in input string
         * Used to printing line segment points in original order
         */
        public boolean isFirst = false;

        /**
         * Second point in line segment
         */
        public Point next;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        /**
         * Links second point in line segment with this
         *
         * @param p second point in line segment
         */
        public void linkPoint(Point p) {
            next = p;
            p.next = this;
        }

        @Override
        public String toString() {
            return x + " " + y;
        }

        @Override
        public int compareTo(Point point) {
            int x = Integer.compare(this.x, point.x);
            if (x != 0) {
                return x;
            }
            return Integer.compare(this.y, point.y);
        }
    }

    /**
     * Check is the point between endpoints of the line segment in 2 axes simultaneously.
     *
     * @param i first line segment endpoint
     * @param j second line segment endpoint
     * @param k point
     * @return point on segment or no
     */
    private static boolean onSegment(Point i, Point j, Point k) {
        return Math.min(i.x, j.x) <= k.x &&
                k.x <= Math.max(i.x, j.x) &&
                Math.min(i.y, j.y) <= k.y &&
                k.y <= Math.max(i.y, j.y);
    }

    /**
     * Returns result of cross-product of vectors (k - i) and (j - i).
     * This value useful when we check 2 line segments intersection.
     *
     * @param i first point
     * @param j second point
     * @param k third point
     * @return result of cross-product of vectors (k - i) and (j - i)
     */
    private static int direction(Point i, Point j, Point k) {
        return (j.x - i.x) * (k.y - i.y) - (k.x - i.x) * (j.y - i.y);
    }

    /**
     * Check two line segments intersection.
     *
     * @param p1 any point of first line segment
     * @param p3 any point of second line segment
     * @return are they intersect or no
     */
    private static boolean isIntersect(Point p1, Point p3) {
        Point p2 = p1.next;
        Point p4 = p3.next;

        int d1 = direction(p3, p4, p1);
        int d2 = direction(p3, p4, p2);
        int d3 = direction(p1, p2, p3);
        int d4 = direction(p1, p2, p4);

        if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
                ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))
        ) return true;

        if (d1 == 0 && onSegment(p3, p4, p1)) return true;

        if (d2 == 0 && onSegment(p1, p4, p2)) return true;

        if (d3 == 0 && onSegment(p1, p2, p3)) return true;

        return d4 == 0 && onSegment(p1, p2, p4);
    }

    /**
     * Prints result if there is an intersection of 2 line segments.
     *
     * @param p1 endpoint of first line segment
     * @param p2 endpoint of second line segment
     */
    private static void printIntersection(Point p1, Point p2) {
        System.out.println("INTERSECTION");
        if (p1.isFirst) {
            System.out.println(p1 + " " + p1.next);
        } else {
            System.out.println(p1.next + " " + p1);
        }
        if (p2.isFirst) {
            System.out.println(p2 + " " + p2.next);
        } else {
            System.out.println(p2.next + " " + p2);
        }
    }

    /**
     * Check any two line segment intersection in given line segments.
     *
     * @param endPoints array of line segment's endpoints
     */
    private static void sweepLine(Point[] endPoints) {
        QuickSort.quickSort(endPoints);
        AVLTree<Point> tree = new AVLTree<>();

        for (Point point : endPoints) {
            if (point.isLeft) {
                tree.insert(point);

                AVLTree.Node<Point> insertedNode = tree.search(point);
                if (insertedNode.count > 1) {
                    printIntersection(point, insertedNode.key);
                    return;
                }

                AVLTree.Node<Point> node = tree.predecessor(insertedNode);
                if (node != null) {
                    if (isIntersect(point, node.key)) {
                        printIntersection(point, node.key);
                        return;
                    }
                }

                node = tree.successor(insertedNode);
                if (node != null) {
                    if (isIntersect(point, node.key)) {
                        printIntersection(point, node.key);
                        return;
                    }
                }
            } else {
                AVLTree.Node<Point> node = tree.search(point.next);
                AVLTree.Node<Point> predecessor = tree.predecessor(node);
                AVLTree.Node<Point> successor = tree.successor(node);

                if (predecessor != null && successor != null) {
                    if (isIntersect(predecessor.key, successor.key)) {
                        printIntersection(predecessor.key, successor.key);
                        return;
                    }
                }

                tree.remove(point.next);
            }
        }

        System.out.println("NO INTERSECTIONS");
    }

    /**
     * Read and return array of line segment endpoints.
     *
     * @return array of line segment endpoints
     */
    private static Point[] readEndPoints() {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        Point[] endPoints = new Point[2 * n];
        for (int i = 0; i < endPoints.length; i++) {
            String[] nums = scanner.nextLine().split(" ");
            Point p1 = new Point(Integer.parseInt(nums[0]), Integer.parseInt(nums[1]));
            p1.isFirst = true;
            Point p2 = new Point(Integer.parseInt(nums[2]), Integer.parseInt(nums[3]));
            p1.linkPoint(p2);
            if (p1.compareTo(p2) < 0) {
                p1.isLeft = true;
                endPoints[i] = p1;
                endPoints[i + 1] = p2;
            } else {
                p2.isLeft = true;
                endPoints[i] = p2;
                endPoints[i + 1] = p1;
            }
            i++;
        }
        return endPoints;
    }

    public static void main(String[] args) {
        Point[] endPoints = readEndPoints();
        sweepLine(endPoints);
    }
}

/**
 * Class containing quicksort sorting algorithm for arrays.
 * <p>
 * Algorithm taken from Cormen's book and modified (added pivot randomizing).
 * Best time complexity  - O(n*logn).
 * Worst time complexity - O(n^2).
 * Randomized pivot used. Therefore, expected time complexity - O(n*logn).
 * Array sorted in place.
 * This algorithm is unstable.
 *
 * @author Abuzyar Tazetdinov
 */
class QuickSort {
    /**
     * Sorts array in ascending order using T's compareTo() method.
     * Works only with Comparable types.
     *
     * @param A   array.
     * @param <T> array elements type.
     */
    public static <T extends Comparable<T>> void quickSort(T[] A) {
        quickSort(A, 0, A.length - 1, Comparable::compareTo);
    }

    /**
     * Sorts array using provided comparator.
     * Can sort non-comparable types using custom comparator.
     *
     * @param A          array.
     * @param <T>        array elements type.
     * @param comparator custom comparator for comparing elements (should implement standard comparator's contract).
     */
    public static <T> void quickSort(T[] A, Comparator<T> comparator) {
        quickSort(A, 0, A.length - 1, comparator);
    }

    /**
     * Sorts array using T's compareTo() method in given bounds.
     *
     * @param A   array.
     * @param <T> array elements type.
     * @param p   left  array bound.
     * @param r   right array bound.
     */
    public static <T extends Comparable<T>> void quickSort(T[] A, int p, int r) {
        quickSort(A, p, r, Comparable<T>::compareTo);
    }

    /**
     * Sorts array using provided comparator in given bounds.
     * Can sort non-comparable types using custom comparator.
     *
     * @param A          array.
     * @param <T>        array elements type.
     * @param p          left  array bound.
     * @param r          right array bound.
     * @param comparator custom comparator for comparing elements.
     */
    public static <T> void quickSort(T[] A, int p, int r, Comparator<T> comparator) {
        if (p < r) {
            int q = partition(A, p, r, comparator);
            quickSort(A, p, q - 1, comparator);
            quickSort(A, q + 1, r, comparator);
        }
    }

    /**
     * Organize array in 3 parts: lower than pivot, equal to pivot, greater than pivot.
     * Splitting done in-place.
     *
     * @param A          array.
     * @param <T>        array elements type.
     * @param p          left  array bound.
     * @param r          right array bound.
     * @param comparator custom comparator for comparing elements.
     * @return index of pivot after partitioning.
     */
    private static <T> int partition(T[] A, int p, int r, Comparator<T> comparator) {
        Random random = new Random();
        int i = p + random.nextInt(r - p + 1);
        swap(A, i, r);
        T x = A[r];
        i = p - 1;
        for (int j = p; j <= r - 1; j++) {
            if (comparator.compare(A[j], x) <= 0) {
                i++;
                swap(A, i, j);
            }
        }
        swap(A, i + 1, r);
        return i + 1;
    }

    /**
     * Swap 2 elements in array.
     *
     * @param A   array.
     * @param <T> array elements type.
     * @param i   first index.
     * @param j   second index.
     */
    private static <T> void swap(T[] A, int i, int j) {
        T tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    }
}

/**
 * Implementation of the AVL-tree with duplicate keys.
 * By-default compare keys using Comparable::compareTo.
 * Can compare keys with the provided comparator.
 * <p>
 * Class contains methods which should be called passing tree's root and many methods returns new tree's root.
 * These methods are hidden for users and overridden with suitable wrappers for improving usability.
 * <p>
 * Main concepts taken from https://en.wikipedia.org/wiki/AVL_tree
 *
 * @param <T> containing elements' type
 * @author Abuzyar Tazetdinov
 */
class AVLTree<T extends Comparable<T>> {
    /**
     * Class representing tree nodes.
     *
     * @param <T> node's key type
     */
    public static class Node<T extends Comparable<T>> {
        public T key;
        public int height = 1;
        public Node<T> left = null;
        public Node<T> right = null;
        public Node<T> parent;
        public int count = 1;

        public Node(T k) {
            key = k;
        }
    }

    private Node<T> root = null;
    private Comparator<T> comparator = Comparable::compareTo;

    public AVLTree() {
    }

    public AVLTree(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    /**
     * Returns height of the node.
     * Used for avoiding many checks for node is null or not.
     * Time complexity - O(1).
     *
     * @param node node
     * @param <T>  node's type
     * @return node's height or 0, if node is null
     */
    private static <T extends Comparable<T>> int height(Node<T> node) {
        return node != null ? node.height : 0;
    }

    /**
     * Returns balance factor for given node.
     * Time complexity - O(1).
     *
     * @param node node
     * @param <T>  node's type
     * @return balance factor
     */
    private static <T extends Comparable<T>> int bFactor(Node<T> node) {
        return height(node.right) - height(node.left);
    }

    /**
     * Calculate and set height for given node.
     * Time complexity - O(1).
     *
     * @param node node
     * @param <T>  node's type
     */
    private static <T extends Comparable<T>> void updateHeight(Node<T> node) {
        node.height = Math.max(height(node.left), height(node.right)) + 1;
    }

    /**
     * Right rotation around the given node.
     * Time complexity - O(1).
     *
     * @param oldRoot root of tree what will be rotated
     * @return root of the tree after rotation
     */
    private Node<T> rotateRight(Node<T> oldRoot) {
        Node<T> newRoot = oldRoot.left;
        oldRoot.left = newRoot.right;
        newRoot.right = oldRoot;
        if (oldRoot.left != null) {
            oldRoot.left.parent = oldRoot;
        }
        oldRoot.parent = newRoot;
        updateHeight(oldRoot);
        updateHeight(newRoot);
        return newRoot;
    }

    /**
     * Left rotation around the given node.
     * Time complexity - O(1).
     *
     * @param oldRoot root of tree what will be rotated
     * @return root of the tree after rotation
     */
    private Node<T> rotateLeft(Node<T> oldRoot) {
        Node<T> newRoot = oldRoot.right;
        oldRoot.right = newRoot.left;
        newRoot.left = oldRoot;
        if (oldRoot.right != null) {
            oldRoot.right.parent = oldRoot;
        }
        oldRoot.parent = newRoot;
        updateHeight(oldRoot);
        updateHeight(newRoot);
        return newRoot;
    }

    /**
     * Balancing tree with provided root.
     * Time complexity - O(1).
     *
     * @param node root of the tree for balancing
     * @return root of the tree after balancing
     */
    private Node<T> balance(Node<T> node) {
        updateHeight(node);
        if (bFactor(node) == 2) {
            if (bFactor(node.right) < 0) {
                node.right = rotateRight(node.right);
            }
            return rotateLeft(node);
        }
        if (bFactor(node) == -2) {
            if (bFactor(node.left) > 0) {
                node.left = rotateLeft(node.left);
            }
            return rotateRight(node);
        }
        return node; // not need to balance
    }

    /**
     * Recursive function for inserting key to tree with provided root.
     * Time complexity - O(logn).
     *
     * @param node root of the tree
     * @param k    key
     * @return root of the tree after balancing
     */
    private Node<T> insert(Node<T> node, T k) {
        if (node == null) {
            node = new Node<>(k);
        } else if (comparator.compare(k, node.key) < 0) {
            if (node.left != null) {
                node.left = insert(node.left, k);
            } else {
                node.left = new Node<>(k);
            }
            node.left.parent = node;
        } else if (comparator.compare(k, node.key) > 0) {
            if (node.right != null) {
                node.right = insert(node.right, k);
            } else {
                node.right = new Node<>(k);
            }
            node.right.parent = node;
        } else if (comparator.compare(k, node.key) == 0) {
            node.count++;
            return balance(node);
        }
        return balance(node);
    }

    /**
     * Inserts key to the tree.
     * Time complexity - O(logn).
     *
     * @param k key
     */
    public void insert(T k) {
        root = insert(root, k);
        root.parent = null;
    }

    /**
     * Search and returns node with minimal key in tree with provided root.
     * Time complexity - O(logn).
     *
     * @param node root of the tree
     * @return node with minimal key or null if node is null
     */
    private Node<T> findMin(Node<T> node) {
        return node.left != null ? findMin(node.left) : node;
    }

    /**
     * Search and returns node with maximal key in tree with provided root.
     * Time complexity - O(logn).
     *
     * @param node root of the tree
     * @return node with maximal key or null if node is null
     */
    private Node<T> findMax(Node<T> node) {
        return node.right != null ? findMax(node.right) : node;
    }

    /**
     * Search and returns node with minimal key in tree.
     * Time complexity - O(logn).
     *
     * @return node with minimal key
     */
    public Node<T> findMin() {
        return findMin(root);
    }

    /**
     * Search and returns node with maximal key in tree.
     * Time complexity - O(logn).
     *
     * @return node with maximal key
     */
    public Node<T> findMax() {
        return findMax(root);
    }

    /**
     * Removes maximal element.
     * Due to balancing operations and fact what we need to rewrite root
     * we cannot call remove(nodeWithMaxEl, nodeWithMaxEl.key) directly
     * and we go to compromise doing 2 operations which cost O(logn).
     */
    public void removeMax() {
        remove(findMax().key);
    }

    /**
     * Removes minimal element.
     * Due to balancing operations and fact what we need to rewrite root
     * we cannot call remove(nodeWithMinEl, nodeWithMinEl.key) directly
     * and we go to compromise doing 2 operations which cost O(logn).
     */
    public void removeMin() {
        remove(findMin().key);
    }

    /**
     * Remove node with minimal key in tree with provided root.
     * ONLY for INTERNAL using when we remove node.
     * Cannot be used directly, because it is special function called only when we swap nodes.
     * Time complexity - O(logn).
     *
     * @param node root of the tree
     * @return new root of the tree after balancing
     */
    private Node<T> removeMinForRemove(Node<T> node) {
        if (node.left == null) {
            return node.right;
        }
        node.left = removeMinForRemove(node.left);
        return balance(node);
    }

    /**
     * Remove node with provided key in tree with provided root.
     * Time complexity - O(logn).
     *
     * @param node root of the tree
     * @param k    key for removing
     * @return new root of the tree after balancing
     */
    private Node<T> remove(Node<T> node, T k) {
        if (node == null) {
            return null;
        }
        if (comparator.compare(k, node.key) < 0) {
            node.left = remove(node.left, k);
        } else if (comparator.compare(k, node.key) > 0) {
            node.right = remove(node.right, k);
        } else { // when node contains key
            if (node.count != 1) {
                node.count--;
                return root;
            }
            if (node.right == null) {
                return node.left;
            }
            Node<T> min = findMin(node.right);
            min.right = removeMinForRemove(node.right);
            min.left = node.left;
            min.parent = node.parent;
            if (min.left != null) {
                min.left.parent = min;
            }
            if (min.right != null) {
                min.right.parent = min;
            }
            return balance(min);
        }
        return balance(node);
    }

    /**
     * Remove node with provided key from the tree.
     * Time complexity - O(logn).
     *
     * @param k key for removing
     */
    public void remove(T k) {
        root = remove(root, k);
        if (root != null) {
            root.parent = null;
        }
    }

    /**
     * Returns node with provided key in tree with provided root.
     * Time complexity - O(logn).
     *
     * @param node root of the tree
     * @param key  key for search
     * @return node with provided key or null if such node does not exists
     */
    private Node<T> search(Node<T> node, T key) {
        if (node == null) {
            return null;
        }
        if (comparator.compare(key, node.key) == 0) {
            return node;
        } else if (comparator.compare(key, node.key) < 0) {
            return search(node.left, key);
        } else {
            return search(node.right, key);
        }
    }

    /**
     * Returns node with provided key.
     * Time complexity - O(logn).
     *
     * @param key key for search
     * @return node with provided key or null if such node does not exists
     */
    public Node<T> search(T key) {
        return search(root, key);
    }

    /**
     * Returns node which value is predecessor of key from a given node.
     * Time complexity - O(logn).
     *
     * @param node node
     * @return node or null if predecessor does not exists
     */
    public Node<T> predecessor(Node<T> node) {
        if (node.left != null) {
            return findMax(node.left);
        }
        Node<T> y = node.parent;
        while (y != null && node == y.left) {
            node = y;
            y = y.parent;
        }
        return y;
    }

    /**
     * Returns node which value is successor of key from a given node.
     * Time complexity - O(logn).
     *
     * @param node node
     * @return node or null if successor does not exists
     */
    public Node<T> successor(Node<T> node) {
        if (node.right != null) {
            return findMin(node.right);
        }
        Node<T> y = node.parent;
        while (y != null && node == y.right) {
            node = y;
            y = y.parent;
        }
        return y;
    }

    /**
     * Recursive method for tree printing.
     * Time complexity - O(n).
     *
     * @param node  root of the tree
     * @param level level of indentation while printing
     */
    private void print(Node<T> node, int level) {
        if (level != 0) {
            for (int i = 0; i < level - 1; i++) {
                System.out.print("   ");
            }
            System.out.print("└──");
        }
        System.out.println(node.key + " (" + node.count + ")");
        if (node.left != null) {
            print(node.left, level + 1);
        }
        if (node.right != null) {
            print(node.right, level + 1);
        }
    }

    /**
     * Print tree.
     * Time complexity - O(n).
     */
    public void print() {
        if (root != null) {
            print(root, 0);
        }
    }
}

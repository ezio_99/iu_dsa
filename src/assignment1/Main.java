package assignment1;

public class Main {
    static DoublyLinkedList<Integer> list = new DoublyLinkedList<>();

    private static void print() {
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();
    }

    private static void get(int i) {
        System.out.println(list.get(i));
    }

    public static void main(String[] args) {
        list.add(0, 2);
        list.addFirst(1);
        list.addLast(3);
        print();
        list.deleteFirst();
        print();
        list.deleteLast();
        print();
        list.addLast(2);
        list.addLast(2);
        list.addLast(2);
        list.addLast(3);
        print();
        list.delete(2);
        print();
        get(3);
        list.set(3, 5);
        print();
        list.delete(Integer.valueOf(2));
        print();
    }
}

package assignment1;

import java.util.*;

// https://codeforces.com/group/3ZU2JJw8vQ/contest/269072/submission/71263823

/**
 * Class for solving Balanced Parentheses task
 *
 * @autor Abuzyar Tazetdinov
 */
public class BalancedParentheses {
    /**
     * Stack, where we store our tokens
     */
    private static Stack<Character> tokens = new Stack<>();

    /**
     * HashMap, where we store pair for token
     */
    private static HashMap<Character, Character> tokenPairs = new HashMap<>() {{
        put('(', ')');
        put('[', ']');
        put('{', '}');
        put(')', '(');
        put(']', '[');
        put('}', '{');
    }};

    /**
     * HashSet with opening tokens
     */
    private static HashSet<Character> openingSymbols = new HashSet<>() {{
        add('(');
        add('[');
        add('{');
    }};

    /**
     * HashSet with closing tokens
     */
    private static HashSet<Character> closingSymbols = new HashSet<>() {{
        add(')');
        add(']');
        add('}');
    }};

    /**
     * Flag, what indicates no errors while parsing
     */
    private static boolean flag = true;

    /**
     * Checks balancing of tokens in string, if find errors - print error message
     *
     * @param string       - input string
     * @param stringNumber - string number
     */
    private static void processLine(String string, int stringNumber) {
        for (int j = 0; j < string.length() && flag; j++) { // We iterate by every char in string
            char token = string.charAt(j);
            if (closingSymbols.contains(token)) { // If token is closing
                try {
                    if (tokens.peek() != tokenPairs.get(token)) { // If we have unclosed tokens
                        System.out.printf("Error in line %d, column %d: expected '%c', but got '%c'.",
                                stringNumber, j + 1, tokenPairs.get(tokens.peek()), token);
                        flag = false;
                    } else { // Token closed
                        tokens.pop();
                    }
                } catch (EmptyStackException e) { // Closing without opening
                    System.out.printf("Error in line %d, column %d: unexpected closing '%c'.", stringNumber, j + 1, token);
                    flag = false;
                }
            } else if (openingSymbols.contains(token)) { // New opening token
                tokens.push(token);
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine()) + 1;
        String string = null;
        for (int i = 1; i < n && flag; i++) { // Processing each string; if we have error, we leave cycle
            string = scanner.nextLine();
            processLine(string, i);
        }
        if (tokens.isEmpty() && flag) { // If all is OK
            System.out.println("Input is properly balanced.");
        } else if (!tokens.isEmpty() && flag && string != null) { // If we have unclosed tokens
            System.out.printf("Error in line %d, column %d: expected '%c', but got end of input.",
                    n - 1, string.length(), tokenPairs.get(tokens.peek()));
        }
        scanner.close();
    }
}

/**
 * Interface of Stack ADT
 *
 * @autor Abuzyar Tazetdinov
 */
interface StackADT<T> {
    /**
     * Check stack is empty or not
     */
    boolean isEmpty();

    /**
     * Add value to top of stack
     *
     * @param e - value
     */
    void push(T e);

    /**
     * Return the value from top of stack
     */
    T peek();

    /**
     * Delete and return value from top of stack
     */
    T pop();
}

/**
 * Implementation of Stack ADT
 *
 * @autor Abuzyar Tazetdinov
 */
class Stack<T> implements StackADT<T> {
    /**
     * Storage for values in stack
     */
    private LinkedList<T> elements = new LinkedList<>();

    /**
     * Check stack is empty or not
     * Time complexity - O(1)
     */
    public boolean isEmpty() {
        return elements.size() == 0;
    }

    /**
     * Add value to top of stack
     * Time complexity - O(1)
     *
     * @param e - value
     */
    @Override
    public void push(T e) {
        elements.addFirst(e);
    }

    /**
     * Delete and return value from top of stack
     * Time complexity - O(1)
     */
    public T pop() {
        if (elements.size() == 0) {
            throw new EmptyStackException();
        }
        T e = elements.get(0);
        elements.removeFirst();
        return e;
    }

    /**
     * Return the value from top of stack
     */
    public T peek() {
        if (elements.size() == 0) {
            throw new EmptyStackException();
        }
        return elements.getFirst();
    }
}

package assignment1;

import java.util.*;

// https://codeforces.com/group/3ZU2JJw8vQ/contest/269072/submission/71263965

/**
 * Class for solving task Phonebook
 * @author Abuzyar Tazetdinov
 */
public class PhoneBook {
    /** Storage for contacts */
    private static HashMap<String, LinkedList<String>> contacts = new HashMap<>();

    /**
     * Add phone number(s) to contact
     * @param input - String in format "<Name>,<Phone1>,<Phone2>,..."
     */
    private static void addContact(String input) {
        String[] data = input.split(",");
        if (!contacts.containsKey(data[0])) { // Create list for numbers if contact doesn't exist
            contacts.put(data[0], new LinkedList<>());
        }
        // We add existing contacts to HashSet for efficient checking doubling numbers
        HashSet<String> existingContacts = new HashSet<>(contacts.get(data[0]));
        for (int i = 1; i < data.length; i++) { // If contact doesn't exist, we add it to list for this user
            if (!existingContacts.contains(data[i])) {
                contacts.get(data[0]).add(data[i]);
            }
        }
    }

    /**
     * Delete specified phone number(s) from contact or all numbers if they not specified
     * @param input - String in format "<Name>" or "<Name>,<Phone1>,<Phone2>,..."
     */
    private static void deleteContact(String input) {
        String[] data = input.split(",");
        if (contacts.containsKey(data[0])) {
            if (data.length == 1) { // Delete all numbers
                contacts.remove(data[0]);
            } else {
                // We add contacts to delete to HashSet for efficient checking what we want to delete
                HashSet<String> contactsToDelete = new HashSet<>();
                for (int i = 1; i < data.length; i++) {
                    contactsToDelete.add(data[i]);
                }
                int i = 0;
                while (contacts.get(data[0]).size() > i) {
                    // If contact in list in our HashSet, we delete it
                    if (contactsToDelete.contains(contacts.get(data[0]).get(i))) {
                        contacts.get(data[0]).remove(i);
                    } else {
                        i++;
                    }
                }
                if (contacts.get(data[0]).size() == 0) { // If user haven't phone numbers, we delete list for them
                    contacts.remove(data[0]);
                }
            }
        }
    }

    /**
     * Print phone numbers for user if they exist
     * @param input - String in format "<Name>"
     */
    private static void findContact(String input) {
        if (!contacts.containsKey(input)) { // User doesn't exist
            System.out.println("No contact info found for " + input);
        } else {
            if (contacts.get(input).size() == 0) { // User haven't phone numbers
                System.out.println("No contact info found for " + input);
            } else { // User exists and have phone numbers
                System.out.print("Found " + contacts.get(input).size() + " phone numbers for " + input + ":");
                for (String phoneNumber : contacts.get(input)) {
                    System.out.print(" " + phoneNumber);
                }
                System.out.println();
            }
        }
    }

    /**
     * Execute user's commands
     * @param command - String in format "ADD/DELETE/FIND <Name>[,<Phone1>,<Phone2>,...]"
     */
    private static void execute(String command) {
        int firstBlank = command.indexOf(" "); // Find first blank's index for extracting action and rest input
        String action = command.substring(0, firstBlank); // Action - ADD/DELETE/FIND
        String input = command.substring(firstBlank + 1); // Rest input - <Name>[,<Phone1>,<Phone2>,...]
        switch (action) { // Executing command for given action
            case "ADD":
                addContact(input);
                break;
            case "DELETE":
                deleteContact(input);
                break;
            case "FIND":
                findContact(input);
                break;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine()); // Count of commands
        for (int i = 0; i < n; i++) {                 // Executing n commands
            String command = scanner.nextLine();
            execute(command.trim());
        }
        scanner.close();
    }
}

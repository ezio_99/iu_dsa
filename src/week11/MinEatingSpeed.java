//package week11;
//
//import java.util.Scanner;
//
//public class MinEatingSpeed {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int H = scanner.nextInt();
//        int N = scanner.nextInt();
//        int[] piles = new int[N];
//        for (int i = 0; i < N; i++) {
//            piles[i] = scanner.nextInt();
//        }
//        Solution solution = new Solution();
//        System.out.println(solution.minEatingSpeed(piles, H));
//    }
//}
//
//class Solution {
//    public int minEatingSpeed(int[] piles, int H) {
//        int start = 1;
//        int end = 1000000000;
//        while (start < end) {
//            int mid = (start + end) / 2;
//            if (!isCanEat(piles, H, mid)) {
//                start = mid + 1;
//            } else {
//                end = mid;
//            }
//        }
//        return start;
//    }
//
//    public boolean isCanEat(int[] piles, int H, int K) {
//        int time = 0;
//        for (int p: piles) {
//            if (p <= K) {
//                ++time;
//            } else {
//                if (p % K > 0) ++time;
//                time += p / K;
//            }
//        }
//        return time <= H;
//    }
//}

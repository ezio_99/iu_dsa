//package week11;
//
//import java.util.*;
//
//public class CourseScheduleII {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int N = scanner.nextInt();
//        int C = scanner.nextInt();
//        int[][] prerequisites = new int[N][2];
//        for (int i = 0; i < N; ++i) {
//            prerequisites[i] = new int[]{scanner.nextInt(), scanner.nextInt()};
//        }
//        Solution1 solution = new Solution1();
//        System.out.println(Arrays.toString(solution.findOrder(C, prerequisites)));
//    }
//}
//
//class Solution1 {
//    public int[] findOrder(int numCourses, int[][] prerequisites) {
//        TopologicGraph graph = new TopologicGraph(numCourses, prerequisites);
//        return graph.topologicalSort();
//    }
//}
//
//class TopologicGraph {
//    private static class LoopException extends RuntimeException {
//        public LoopException(String s) {
//            super(s);
//        }
//    }
//
//    private static class TopologicalSortResult {
//        private int index = 0;
//        public int[] values;
//
//        TopologicalSortResult(int N) {
//            values = new int[N];
//        }
//
//        void add(int i) {
//            values[index++] = i;
//        }
//    }
//
//    int[] states;
//    List<List<Integer>> edges;
//    TopologicalSortResult result;
//
//    private static final int WHITE = 0;
//    private static final int GRAY = 1;
//    private static final int BLACK = 2;
//
//    public TopologicGraph(int N, int[][] prerequisites) {
//        states = new int[N];
//        result = new TopologicalSortResult(N);
//        edges = new ArrayList<>(N);
//        for (int i = 0; i < N; i++) {
//            edges.add(new ArrayList<>());
//        }
//
//        for (int[] prerequisite : prerequisites) {
//            edges.get(prerequisite[0]).add(prerequisite[1]);
//        }
//    }
//
//    private void DFS(int node) {
//        states[node] = GRAY;
//
//        for (Integer neighbor : edges.get(node)) {
//            if (states[neighbor] == GRAY) {
//                throw new LoopException(String.format("Loop from %d to %d", node, neighbor));
//            }
//            if (states[neighbor] == WHITE) {
//                DFS(neighbor);
//            }
//        }
//
//        states[node] = BLACK;
//        result.add(node);
//    }
//
//    int[] topologicalSort() {
//        for (int i = 0; i < states.length; i++) {
//            if (states[i] == WHITE) {
//                try {
//                    DFS(i);
//                } catch (LoopException e) {
//                    return new int[0];
//                }
//            }
//        }
//        return result.values;
//    }
//}

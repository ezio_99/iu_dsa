package week5;

import java.util.Scanner;

public class KnapsackBruteForce {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int W = scanner.nextInt();
        int[] weights = new int[n];
        int[] costs = new int[n];
        for (int i = 0; i < n; i++) {
            weights[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            costs[i] = scanner.nextInt();
        }
        int M = (int) Math.pow(2, n) - 1;
        String result = "";
        int max = -1;
        int w = W + 1;

        while (M >= 0) {
            int sum = 0;
            int weight = 0;
            String m = Integer.toBinaryString(M);
            StringBuilder r = new StringBuilder();
            int p = n - m.length();
            for (int i = n - m.length(); i < n; i++) {
                if (m.charAt(i - p) == '1') {
                    int newWeight = weight + weights[i];
                    if (newWeight <= W) {
                        sum += costs[i];
                        weight = newWeight;
                        r.append(i+1).append(" ");
                    } else {
                        break;
                    }
                }
            }
            if (max < sum || (max == sum && weight <= w)) {
                result = r.substring(0, r.length() - 1);
                max = sum;
                w = weight;
            }
            M--;
        }

        System.out.println(result.split(" ").length);
        System.out.println(result);
    }
}

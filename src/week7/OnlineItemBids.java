//package week7;
//
//import java.util.ArrayDeque;
//import java.util.Random;
//import java.util.Scanner;
//
//public class OnlineItemBids {
//    public static void quickSort(Lot[] A, int p, int r) {
//        if (p < r) {
//            int q = partition(A, p, r);
//            quickSort(A, p, q - 1);
//            quickSort(A, q + 1, r);
//        }
//    }
//
//    public static int partition(Lot[] A, int p, int r) {
//        Random random = new Random();
//        int i = p + random.nextInt(r + 1);
//        swap(A, i, r);
//        Lot x = A[r];
//        i = p - 1;
//        for (int j = p; j <= r - 1; j++) {
//            if (A[j].max < x.max) {
//                i++;
//                swap(A, i, j);
//            }
//        }
//        swap(A, i + 1, r);
//        return i + 1;
//    }
//
//    public static void swap(Lot[] A, int i, int j) {
//        Lot tmp = A[i];
//        A[i] = A[j];
//        A[j] = tmp;
//    }
//
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//        Lot[] lots = new Lot[n];
//        for (int i = 0; i < n; i++) {
//            lots[i] = new Lot(scanner.nextInt(), scanner.nextInt(), i + 1);
//        }
//        Node[] min_buckets = new Node[101];
//        for (int i = 0; i < 101; i++) {
//            min_buckets[i] = new Node();
//        }
//        for (int i = 0; i < n; i++) {
//            int min = scanner.nextInt();
//            int max = scanner.nextInt();
//            Lot lot = new Lot(max, i + 1);
//            min_buckets[min].lots.add(lot);
//        }
//
//        for (int i = 101; i >= 0; i--) {
//            quickSort(lots[i], 0, n - 1);
//        }
//
//        System.out.print(lots[0].originalIndex);
//        for (int i = 1; i < n; i++) {
//            System.out.print(" " + lots[i].originalIndex);
//        }
//    }
//}
//
//class Lot {
////    public int min;
//    public int max;
//    public int originalIndex;
//
//    public Lot(int max, int originalIndex) {
////        this.min = min;
//        this.max = max;
//        this.originalIndex = originalIndex;
//    }
//}

package assignment3;

public class MinimumSpanningForest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number_of_vertices = scanner.nextInt();
        int number_of_edges = scanner.nextInt();

        Graph<Integer, Integer> graph = new Graph<>();
        for (int i = 1; i <= number_of_vertices; ++i) {
            graph.insertVertex(i);
        }

        for (int i = 0; i < number_of_edges; ++i) {
            graph.insertEdge(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        }
    }
}

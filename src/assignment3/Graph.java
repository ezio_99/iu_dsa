package assignment3;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Graph<V, E> {
    private final HashMap<V, List<Pair<V, E>>> adjacencyLists = new HashMap<>();

    public Graph() {
    }

    /**
     * Returns opposite vertex for given vertex and edge.
     * Time complexity - O(|V|), where V - set of connected to this vertex vertices.
     *
     * @param vertex
     * @param edge
     * @return
     */
    public V opposite(V vertex, E edge) {
        if (!adjacencyLists.containsKey(vertex)) {
            throw new RuntimeException("Vertex " + vertex + " does not exist in graph");
        }
        for (int i = 0; i < adjacencyLists.get(vertex).size(); ++i) {
            if (adjacencyLists.get(vertex).get(i).second == edge) {
                return adjacencyLists.get(vertex).get(i).first;
            }
        }
        return null;
    }

    /**
     * Check is there connection between given vertices.
     * Time complexity - O(|V|), where V - set of connected to first vertex vertices.
     *
     * @param vertex1
     * @param vertex2
     * @return
     */
    public boolean areAdjacent(V vertex1, V vertex2) {
        if (!adjacencyLists.containsKey(vertex1)) {
            throw new RuntimeException("Vertex " + vertex1 + " does not exist in graph");
        }
        if (!adjacencyLists.containsKey(vertex2)) {
            throw new RuntimeException("Vertex " + vertex2 + " does not exist in graph");
        }
        for (Pair<V, E> pair : adjacencyLists.get(vertex1)) {
            if (pair.first == vertex2) return true;
        }
        return false;
    }

    /**
     * @param vertex
     * @return
     */
    int degree(V vertex) {
        if (!adjacencyLists.containsKey(vertex)) {
            throw new RuntimeException("Vertex " + vertex + " does not exist in graph");
        }
        return adjacencyLists.get(vertex).size();
    }

    /**
     * Insert vertex to graph.
     * Time complexity - O(1).
     *
     * @param vertex
     */
    public void insertVertex(V vertex) {
        if (!adjacencyLists.containsKey(vertex)) {
            adjacencyLists.put(vertex, new ArrayList<>());
        }
    }

    /**
     * @param start
     * @param end
     * @param cost
     */
    public void insertEdge(V start, V end, E cost) {
        // TODO: fix
        insertVertex(start);
        insertVertex(end);
//        if (!adjacencyLists.containsKey(start)) {
//            adjacencyLists.put(start, new ArrayList<>());
//        }
//        if (!adjacencyLists.containsKey(end)) {
//            adjacencyLists.put(end, new ArrayList<>());
//        }
        adjacencyLists.get(start).add(new Pair<>(end, cost));
        adjacencyLists.get(end).add(new Pair<>(start, cost));
    }

    /**
     * Time complexity - O(|V|), where V // TODO
     *
     * @param vertex
     */
    public void removeVertex(V vertex) {
        adjacencyLists.remove(vertex);
        List<Pair<V, E>> list;
        for (V v : adjacencyLists.keySet()) {
            list = adjacencyLists.get(v);
            for (int i = 0; i < list.size(); ++i) {
                if (list.get(i).first == vertex) {
                    list.remove(i);
                    --i;
                }
            }
        }
    }

    /**
     * @param start
     * @param end
     */
    public void removeEdge(V start, V end) {
        if (adjacencyLists.containsKey(start) && adjacencyLists.containsKey(end)) {
            List<Pair<V, E>> list;
            list = adjacencyLists.get(start);
            for (int i = 0; i < list.size(); ++i) {
                if (list.get(i).first == start) {
                    list.remove(i);
                    break;
                }
            }

            list = adjacencyLists.get(end);
            for (int i = 0; i < list.size(); ++i) {
                if (list.get(i).first == start) {
                    list.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * @param vertex
     * @return
     */
    public ArrayList<V> incidentEdges(V vertex) {
        if (!adjacencyLists.containsKey(vertex)) {
            throw new RuntimeException("Vertex " + vertex + " does not exist in graph");
        }
        ArrayList<V> result = new ArrayList<V>();
        for (Pair<V, E> pair : adjacencyLists.get(vertex)) {
            result.add(pair.first);
        }
        return result;
    }

    /**
     * @return
     */
    public Pair<V, V> analyzeConnectivity() {
        if (adjacencyLists.isEmpty()) return new Pair<>(null, null);
        V start = null;
        V end = null;
        for (V v : adjacencyLists.keySet()) {
            if (adjacencyLists.get(v).isEmpty()) {
                end = v;
            } else {
                start = v;
                if (end != null) break;
            }
        }
        if (end == null) return new Pair<>(null, null);

        return new Pair<>(start, end);
    }

    /**
     * @return
     */
    public HashMap<V, Integer> vertexComponents() {
        HashMap<V, Integer> components = new HashMap<>();
        HashSet<V> visitedVertices = new HashSet<>();

        V[] vertices = (V[]) adjacencyLists.keySet().toArray();
        Arrays.sort(vertices);

        int n = 1;
        for (V current : vertices) {
            if (!visitedVertices.contains(current)) {
                DFS(current, visitedVertices, n, components);
                ++n;
            }
        }

        return components;
    }

    private void DFS(V vertex, HashSet<V> visitedNodes, int n, HashMap<V, Integer> components) {
        if (visitedNodes.contains(vertex)) {
            return;
        }
        visitedNodes.add(vertex);
        components.put(vertex, n);
        for (Pair<V, E> pair : adjacencyLists.get(vertex)) {
            DFS(pair.first, visitedNodes, n, components);
        }
    }

    public List<Graph<V, E>> minimumSpanningForest() {

    }
}

class Pair<T, U> {
    T first;
    U second;

    public Pair(T v1, U v2) {
        first = v1;
        second = v2;
    }
}

class Scanner {
    InputStream in;
    char c;

    Scanner(InputStream in) {
        this.in = in;
        nextChar();
    }

    void asserT(boolean e) {
        if (!e) {
            throw new Error();
        }
    }

    void nextChar() {
        try {
            c = (char) in.read();
        } catch (IOException e) {
            throw new Error(e);
        }
    }

    long nextLong() {
        while (true) {
            if ('0' <= c && c <= '9' || c == '-') {
                break;
            }
            asserT(c != -1);
            nextChar();
        }
        long sign = 1;
        if (c == '-') {
            sign = -1;
            nextChar();
        }
        long value = c - '0';
        nextChar();
        while ('0' <= c && c <= '9') {
            value *= 10;
            value += c - '0';
            nextChar();
        }
        value *= sign;
        return value;
    }

    int nextInt() {
        long longValue = nextLong();
        int intValue = (int) longValue;
        asserT(intValue == longValue);
        return intValue;
    }
}

package assignment3;

import java.util.HashMap;

public class ComponentsDictionary {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number_of_vertices = scanner.nextInt();
        int number_of_edges = scanner.nextInt();

        // bool - minimizing memory leakage
        Graph<Integer, Boolean> graph = new Graph<>();
        for (int i = 1; i <= number_of_vertices; ++i) {
            graph.insertVertex(i);
        }

        for (int i = 0; i < number_of_edges; ++i) {
            graph.insertEdge(scanner.nextInt(), scanner.nextInt(), false);
        }

        HashMap<Integer, Integer> components = graph.vertexComponents();
        System.out.print(components.get(1));
        for (int i = 2; i <= components.size(); i++) {
            System.out.print(" " + components.get(i));
        }
    }
}

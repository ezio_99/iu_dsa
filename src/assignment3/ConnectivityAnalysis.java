package assignment3;

public class ConnectivityAnalysis {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number_of_vertices = scanner.nextInt();
        int number_of_edges = scanner.nextInt();

        // bool - minimizing memory leakage
        Graph<Integer, Boolean> graph = new Graph<>();
        for (int i = 1; i <= number_of_vertices; ++i) {
            graph.insertVertex(i);
        }

        for (int i = 0; i < number_of_edges; ++i) {
            graph.insertEdge(scanner.nextInt(), scanner.nextInt(), false);
        }

        Pair<Integer, Integer> unconnectedVertices = graph.analyzeConnectivity();

        if (unconnectedVertices.first == null) {
            System.out.println("GRAPH IS CONNECTED");
        } else {
            System.out.println("VERTICES " + unconnectedVertices.first + " AND "
                    + unconnectedVertices.second + " ARE NOT CONNECTED BY A PATH");
        }
    }
}
